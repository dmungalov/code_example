let instance = null;

export const DEVICE_TYPE_CHANGED_EVENT = 'device.type.changed';

export const VIEWPORT = {
    MOBILE_MIN: 320,
    MOBILE_MID: 479,
    MOBILE_MAX: 767,
    TABLET_MIN: 768,
    TABLET_MAX: 991,
    DESKTOP_MIN: 992,
    DESKTOP_MAX: 1199,
    DESKTOP_LARGE_MIN: 1200,
};

export const DEVICE_TYPE = {
  MOBILE  : 0,
  TABLET  : 1,
  DESKTOP : 2,
};

class AdaptiveService {

  $window = undefined;

  constructor () {
    if (!instance) {
      this.$window = jQuery(window);

      instance = this;

      this.bind();
      this.triggerResizeEvent();
    }
    return instance;
  }

  bind() {
    this.$window.resize(this.triggerResizeEvent);
  }

  triggerResizeEvent = () => {
    let currentType = this.getTypeByViewportSize();
    if(this.deviceType !== currentType) {
      this.deviceType = currentType;
      this.$window.trigger(DEVICE_TYPE_CHANGED_EVENT, {
        type : this.deviceType
      });
    }
  };

  getTypeByViewportSize() {
    if(this.isMobile) {
      return DEVICE_TYPE.MOBILE;
    }

    if(this.isTablet) {
      return DEVICE_TYPE.TABLET;
    }

    if(this.isDesktop) {
      return DEVICE_TYPE.DESKTOP;
    }
  }

  getDeviceType() {
    return this.deviceType;
  }

  get width () {
    return this.$window.width();
  }

  get height() {
    return this.$window.height();
  }

  get isTouch () {
    return jQuery('body').hasClass('mobile');
  }

  get isMobile () {
    return VIEWPORT.MOBILE_MIN >= window.innerWidth || window.innerWidth <= VIEWPORT.MOBILE_MAX;
  }

  get isMobilePhone () {
    return VIEWPORT.MOBILE_MIN >= window.innerWidth || window.innerWidth <= VIEWPORT.MOBILE_MID;
  }

  get isTablet () {
    return VIEWPORT.TABLET_MIN <= window.innerWidth && window.innerWidth <= VIEWPORT.TABLET_MAX;
  }

  get isDesktop () {
    return VIEWPORT.DESKTOP_MIN <= window.innerWidth || window.innerWidth >= VIEWPORT.DESKTOP_MAX;
  }

}

export default new AdaptiveService;