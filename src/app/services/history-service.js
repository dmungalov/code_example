import {searchParamsToObject, objectToSearchParams} from '../helpers/url-helper';
import merge from 'lodash/merge';
import isEqual from 'lodash/isEqual';
import clone from 'lodash/clone';

const IS_HISTORY_API = 'undefined' !== typeof window.history
  && 'undefined' !== typeof window.history.pushState
  && 'undefined' !== typeof window.history.replaceState;
let instance = null;

class HistoryService {
  constructor () {
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  setState (data) {
    this.processState('pushState', data);
  }

  replaceState (data) {
    this.processState('replaceState', data);
  }

  processState (method, data) {
    let oldState = this.getState(),
      newState = this.getNewState(oldState, data);

    if (isEqual(oldState, newState) || !IS_HISTORY_API) {
      return;
    }

    history[method](newState, 'test', this.getUrl(newState));
  }

  getUrl (state) {
    return window.location.pathname + '?' + objectToSearchParams(state);
  }

  getState () {
    return window.location.search ? searchParamsToObject(window.location.search) : {};
  }

  getNewState (oldState, data) {
    return this.allToString(merge(clone(oldState), data));
  }

  allToString (obj) {
    if ('object' !== typeof obj) {
      return obj;
    }

    Object.keys(obj).forEach((param) => {
      obj[param] = obj[param] + '';
    });

    return obj;
  }
}

export default new HistoryService;