let instance = null;

export const cssClass = {
    MODAL_OPEN: 'modal-open',
};
export const selector = {
    LAYOUT: '.layout',
};
export const event = {
    MODAL_SHOW: 'modal.show.modalService',
    MODAL_HIDE: 'modal.hide.modalService',
    SIDEBAR_SHOW: 'sidebar:open.st.sidebar.modalService',
    SIDEBAR_HIDE: 'sidebar:close.st.sidebar.modalService',
};

class ModalService {
  constructor () {
    if (!instance) {
      instance = this;
    }

    this.scrollTop = 0;
    this.$body = jQuery('body');
    this.$layout = jQuery(selector.LAYOUT);

    this.init();
    return instance;
  }

  init () {
      this.bind();
  }

  bind() {
      jQuery(document).off(event.MODAL_SHOW).on(event.MODAL_SHOW, this.showHandler);
      jQuery(document).off(event.MODAL_HIDE).on(event.MODAL_HIDE, this.hideHandler);

      jQuery(document).off(event.SIDEBAR_SHOW).on(event.SIDEBAR_SHOW, this.showHandler);
      jQuery(document).off(event.SIDEBAR_HIDE).on(event.SIDEBAR_HIDE, this.hideHandler);
  }

  showHandler = () => {
      let scrollTop = window.pageYOffset;
      this.scrollTop = scrollTop;

      this.$body.addClass(cssClass.MODAL_OPEN);
      this.$layout.css({top: -scrollTop});
      window.scrollTo(0, 0);
  };

  hideHandler = () => {
      this.$body.removeClass(cssClass.MODAL_OPEN);
      this.$layout.css({top: 'auto'});
      window.scrollTo(0, this.scrollTop);
  };
}

export default new ModalService;