/**
 * common styles
 */
import 'styles/index.scss';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import BoxLayout from './react/components/BoxLayout';
import Registration from './react/features/Registration';
import { searchParamsToObject } from './helpers/url-helper';

document.addEventListener('DOMContentLoaded', () => {
  const urlParams = searchParamsToObject();
  ReactDOM.render(
    <BoxLayout>
      <Registration type={urlParams.type} />
    </BoxLayout>, 
    document.getElementById('app')
  );
});