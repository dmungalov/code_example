import * as axios from 'axios';

export const API_VERSION = 1;


const request = axios.create({
  baseURL: '',
  headers: {'Accept-Version': API_VERSION},
});
export default request;


/* helpers */
export function objectToFormData (object) {
  let form_data = new FormData();

  for (let key in object) {
    form_data.append(key, object[key]);
  }

  return form_data;
}