import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
export const cx = classNames.bind(style);

interface IProps {
  color?: string;
  className?: string;
}

class Loader extends React.PureComponent<IProps> {
  render () {
    const { color, className } = this.props;
    const items = Array.apply(null, {length: 12}).map(Number.call, Number);

    return (
      <div className={cx(className, 'lds-css', 'ng-scope')}>
        <div className={cx('lds-spinner')} style={{width: '100%', height: '100%'}}>
          {items.map(item => <div key={item} style={{backgroundColor: color || '#000000'}}/>)}
        </div>
      </div>
    );
  }
}

export default Loader;