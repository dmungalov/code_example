import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
export const cx = classNames.bind(style);

export enum Size {
  sm = 'sm', 
  nm = 'nm', 
  md = 'md', 
  lg = 'lg',
};

export enum Align {
  left = 'left',
  center = 'center',
  right = 'right',
}

export enum Weight {
  normal = 'normal',
  bold = 'bold',
}

export enum Style {
  error = 'error',
  warning = 'warning',
}

interface IProps {
  size?: Size;
  align?: Align;
  weight?: Weight;
  style?: Style;
  children: string;
  className?: string;
}

class Text extends React.PureComponent<IProps> {
  render() {
    const { size, align, weight, style, children, className } = this.props;

    return (
      <div className={cx(className, {
        text: true,
        [`text_size_${size}`]: size,
        [`text_align_${align}`]: align,
        [`text_weight_${weight}`]: weight,
        [`text_style_${style}`]: style,
      })}>{children}</div>
    );
  }
}

export default Text;
