import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

export enum RowMod {
  largeMargin = 'large-margin',
}

interface IProps {
  mod?: RowMod;
  className?: string;
  children: any;
}

class Row extends React.PureComponent<IProps> {
  render() {
    const { mod, className, children } = this.props;

    return (
      <div className={cx(className, {
        row: true,
        [`row_${mod}`]: true,
      })}>{children}</div>
    )
  }
}


export default Row;
