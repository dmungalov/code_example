import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface ILink {
  title: string;
  url: string;
}

interface IProps {
  links: ILink[];
  button?: string;
}

class Agreement extends React.Component<IProps> {
  render() {
    const { links } = this.props;
    const button = this.props.button || 'Оформить заказ';

    return (
      <div className={cx('agreement')}>
        <div className={cx('agreement__title')}>Нажимая на кнопку «{button}», вы соглашаетесь:</div>
        {links.map((link, index) => (
          <div key={index} className={cx('agreement__item')}>
            <a className={cx('agreement__link')} target="_blank" href={link.url}>
              - {link.title}
            </a>
          </div>
        ))}
      </div>
    );
  }
}

export default Agreement;
