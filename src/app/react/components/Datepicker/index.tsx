import * as React from 'react';
import * as moment from 'moment';
import BaseDatePicker from 'react-datepicker';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  onChange?: any;
  input?: any;
  includeDates?: any[];
}

class DatePicker extends React.Component<IProps> {
  onChange = date => {
    const { input, onChange } = this.props;
    const handleChange = input ? input.onChange : onChange;
    handleChange && handleChange(moment(date).format('MM-DD-YYYY'));
  };

  render() {
    const { includeDates, input } = this.props;
    const preparedDates = includeDates.map(date => moment(date));
    const selectedDate = input ? moment(input.value) : '';

    return (
      <BaseDatePicker
        onChange={this.onChange}
        includeDates={preparedDates}
        selected={selectedDate}
        locale="ru-Ru"
        className={cx('input')}
      />
    );
  }
}

export default DatePicker;
