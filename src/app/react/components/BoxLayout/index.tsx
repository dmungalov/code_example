import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  className?: string;
}

const BoxLayout: React.SFC<IProps> = props => (
  <div
    className={cx(props.className, {
        'box-layout': true,
    })}
  >
    <div className={cx('box-layout__wrapper')}>
        <div className={cx('box-layout__inner')}>
            <div className={cx('box-layout__content')}>
                {props.children}
            </div>
        </div> 
    </div>  
  </div>
);

export default BoxLayout;
