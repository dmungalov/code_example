import * as React from 'react';
import * as ReactModal from 'react-modal';
import { event as ModalServiceEvents } from '../../../services/modal-service';
import Title, { TitleSize, TitleWeight } from '../../components/Title';
import Text, { Size } from '../../components/Text';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
export const cx = classNames.bind(style);

interface IProps {
  isOpen: boolean;
  children?: any;
  title?: string;
  subtitle?: string;
  className?: string;
  onClose?: () => void;
  scrollPos?: number;
}

declare var jQuery: any;

class Modal extends React.Component<IProps> {

  componentWillReceiveProps(nextProps: IProps) {
    if (this.props.isOpen !== nextProps.isOpen) {
      if (jQuery) {
        jQuery(document).trigger(ModalServiceEvents.MODAL_HIDE);
      }
    }
  }

  onClose = () => {
    const { onClose } = this.props;
    onClose && onClose();
    
    if (jQuery) {
      jQuery(document).trigger(ModalServiceEvents.MODAL_HIDE);
    }
  }

  onOpen = () => {
    window.scrollTo(0, this.props.scrollPos);
    if (jQuery) {
      jQuery(document).trigger(ModalServiceEvents.MODAL_SHOW);
    }
  }

  render () {
    const { children, title, subtitle, className, ...props } = this.props;

    return (
      <ReactModal className={cx('modal__center')}
                  ariaHideApp={false}
                  {...props}
                  onRequestClose={this.onClose}
                  onAfterOpen={this.onOpen}
                  // closeTimeoutMS={200}
                  overlayClassName={cx('modal')}
      >
        <div className={cx(className, 'modal__box')}>
          <div className={cx('modal__header')}>
            <Title size={TitleSize.h3} weight={TitleWeight.bold}>{title}</Title>
            {subtitle && <Text size={Size.nm}>{subtitle}</Text>}
            <div className={cx('modal__close')} onClick={this.onClose} />
          </div>
          <div className={cx('modal__content')}>
            {children}
          </div>
        </div>
        <div className={cx('modal__overlay')} onClick={this.onClose} />
      </ReactModal>
    );
  }
}

export default Modal;