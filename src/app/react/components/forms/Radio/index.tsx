import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

type IProps = {
  type?: string;
  title?: string;
  value: any;
} & FieldRenderProps;

const Radio: React.SFC<IProps> = props => {
  const { input, type, title, children } = props;
  
  return (
    <label className={cx('radio')}>
      <input className={cx('radio__input')} {...input} type={type} />
      <span className={cx('radio__label')}>{title || children}</span>
    </label>
  )
}

export default Radio;