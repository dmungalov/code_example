import * as React from 'react';
import BaseSelect from 'react-select';
import { FieldRenderProps } from 'react-final-form';

import Option from './components/Option';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

export interface IOption {
  label: string;
  value: any;
  disabled?: boolean;
}

type IProps = {
  id: string;
  placeholder?: string;
  options: IOption[];
} & FieldRenderProps;

const Select: React.SFC<IProps> = props => {
  const { input, meta: { error }, placeholder, options, id } = props;

  return (
    <BaseSelect
      instanceId={id}
      className={cx({
        select: true,
        select_error: error,
      })}
      placeholder={placeholder || 'Не выбрано'}
      simpleValue
      searchable={false}
      clearable={false}
      onBlurResetsInput={false}
      onCloseResetsInput={false}
      value={input.value}
      onBlur={input.onBlur}
      onChange={input.onChange}
      onFocus={input.onFocus}
      options={options}
      optionRenderer={Option}
    />
  );
};

export default Select;
