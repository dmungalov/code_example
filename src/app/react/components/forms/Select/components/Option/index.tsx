import * as React from 'react';

interface IProps {
  value: string;
  label: string;
  disabled?: boolean
}

const Option: React.SFC<IProps> = props => (
  <div>
  {props.label}
  {props.disabled && <span className="is-unaviable">недоступно</span>}
</div>
)

export default Option;
