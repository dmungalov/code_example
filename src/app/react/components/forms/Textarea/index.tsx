import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

type IProps = {
  id: string;
  placeholder?: string;
  rows?: number;
} & FieldRenderProps;

const Textarea: React.SFC<IProps> = props => {
  const { input, meta: { touched, error }, placeholder, id, rows } = props;

  return (
    <textarea
      className={cx({
        textarea: true,
        textarea_error: touched && error,
      })}
      {...input}
      id={id}
      placeholder={placeholder}
      rows={rows || 3}
    />
  );
};

export default Textarea;
