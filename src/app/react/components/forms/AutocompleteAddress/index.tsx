import * as React from 'react';
import 'suggestions-jquery';
// import { FieldRenderProps } from 'react-final-form';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

declare var jQuery: any;

interface IProps {
  value?: string;
  id?: string;
  name?: string;
  placeholder?: string;
  onSelect?: (data: any) => void;
  onSelectAddressNothing?: (query: string) => void;
  hint?: string;
  count?: number;
  disabled?: boolean;
  isError: boolean
}

interface IState {
  value?: string;
  isError?: boolean;
}

// TODO: refactoring

class AutocompleteAddress extends React.Component<IProps, IState> {
  field
  $field

  constructor (props) {
      super(props);

      this.state = {
        value: this.props.value || '',
        isError: props.isError,
      };
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState(state => ({
        ...this.state,
        value: nextProps.value,
        isError: nextProps.isError,
      }))
    }
  }

  componentDidMount () {
    const { hint, count, onSelect, onSelectAddressNothing } = this.props;

    this.$field = jQuery(this.field);
    this.$field.suggestions({
      token: '409d514434c45e3787458a8dfba256fef05a33e9',
      type: 'ADDRESS',
      count: count || 5,
      scrollOnFocus: false,
      mobileWidth: 100,
      addon: 'none',
      hint: hint || 'Выберите адрес',
      minChars: 4,
      deferRequestBy: 300,
      onSelect,
      onSelectNothing: onSelectAddressNothing,
    })
  }

  onChange = event => {
    const value = event.target.value

    this.setState(state => ({
      ...this.state,
      value,
    }))
  }

  render () {
    const { placeholder, name, id, disabled } = this.props;
    const { isError } = this.state;

    return (
      <div className={cx('field-address')}>
        <input name={name}
               id={id}
               placeholder={placeholder}
               className={isError ? 'suggestions-input suggestions-input_error' : 'suggestions-input'}
               value={this.state.value}
               onChange={this.onChange}
               disabled={disabled}
               ref={input => this.field = input} />
      </div>
    );
  }
};

export default AutocompleteAddress;
