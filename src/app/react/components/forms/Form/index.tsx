import * as React from 'react';
import { Form as FinalForm } from 'react-final-form';
import createDecorator from './decorators/scrollToFirstError';

import Condition from '../Condition';
import Section from '../Section';
import Field from '../Field';
import FieldLabel from '../FieldLabel';
import FieldError from '../FieldError';

const focusOnErrors = createDecorator();

interface IProps {
  id?: string;
  className?: string;
  initialValues?: any;
  onSubmit: (values: any) => void;
}

class Form extends React.Component<IProps> {
  static Section = Section;
  static Field = Field;
  static FieldLabel = FieldLabel;
  static FieldError = FieldError;
  static Condition = Condition;

  render() {
    const { onSubmit, className, id, initialValues } = this.props;

    return (
      <FinalForm
        decorators={[focusOnErrors]}
        initialValues={initialValues}
        onSubmit={onSubmit}
        render={({ handleSubmit }) => (
          <form id={id} onSubmit={handleSubmit} className={className} noValidate autoComplete="off">
            {this.props.children}
          </form>
        )}
      />
    );
  }
}
export default Form;