import { FormApi, getIn } from 'final-form';

const getAllInputs = () => {
  if (typeof document === 'undefined') {
    return [];
  }
  return Array.prototype.slice
    .call(document.forms)
    .reduce((accumulator, form) => accumulator.concat(Array.prototype.slice.call(form)), []);
};

const createDecorator = (getInputs?) => (form: FormApi) => {
  const isElementInViewport = el => {
    const rect = el.getBoundingClientRect();
    return (
      rect.top - 200 >= 0 &&
      rect.left >= 0 &&
      rect.bottom + 200 <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }
  const scrollToFirstError = errors => {
    if (!getInputs) {
      getInputs = getAllInputs;
    }
    const firstInput = getInputs().find(input => input.name && getIn(errors, input.name));
    if (firstInput && !isElementInViewport(firstInput)) {
      firstInput.scrollIntoView(true);
      window.scrollBy(0, -100);
    }
  };

  const originalSubmit = form.submit;

  // Subscribe to errors, and keep a local copy of them
  let state: { errors?: any; submitErrors?: any } = {};
  const unsubscribe = form.subscribe(
    nextState => {
      state = nextState;
    },
    { errors: true, submitErrors: true },
  );

  // What to do after submit
  const afterSubmit = () => {
    const { errors, submitErrors } = state;
    if (errors && Object.keys(errors).length) {
      scrollToFirstError(errors);
    } else if (submitErrors && Object.keys(submitErrors).length) {
      scrollToFirstError(submitErrors);
    }
  };

  // Rewrite submit function
  form.submit = () => {
    const result = originalSubmit.call(form);
    if (result && typeof result.then === 'function') {
      // async
      result.then(afterSubmit);
    } else {
      // sync
      afterSubmit();
    }
    return result;
  };

  return () => {
    unsubscribe();
    form.submit = originalSubmit;
  };
};

export default createDecorator