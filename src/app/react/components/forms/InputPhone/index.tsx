import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';
import * as MaskedInput from 'react-input-mask/dist/react-input-mask.min';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

type IProps = {
  placeholder?: string;
  id: string;
} & FieldRenderProps;

const InputPhone: React.SFC<IProps> = props => {
  const { input, meta: { touched, error }, placeholder, id } = props;

  return (
    <MaskedInput
      className={cx({
        'input-phone': true,
        'input-phone_error': touched && error,
      })}
      {...input}
      type="tel"
      id={id}
      placeholder={placeholder}
      mask="+79999999999"
    />
  );
};

export default InputPhone;
