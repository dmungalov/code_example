import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

type IProps = {
  placeholder?: string;
  id: string;
  type?: string;
  onFocus?: any;
} & FieldRenderProps;

class InputDate extends React.Component<IProps> {

  onFocus = () => {
    if (this.props.onFocus) {
      this.props.onFocus();  
    }
    this.props.input.onFocus();
  }

  render() {
    const { input, meta: { touched, error }, placeholder, id, type } = this.props;
    return (
      <input
        className={cx({
          input: true,
          input_error: touched && error,
        })}
        {...input}
        onFocus={this.onFocus}
        type={type}
        id={id}
        placeholder={placeholder}
      />
    );
  }
}

export default InputDate;
