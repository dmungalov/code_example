import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  className?: string;
  children: any;
}

const Section: React.SFC<IProps> = props => (
  <div className={cx('form__section', props.className)}>{props.children}</div>
);

export default Section;
