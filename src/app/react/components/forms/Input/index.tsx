import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

type IProps = {
  placeholder?: string;
  id: string;
  type?: string;
  disabled?: boolean;
} & FieldRenderProps;

const Input: React.SFC<IProps> = props => {
  const { input, meta: { touched, error }, placeholder, id, type, disabled } = props;

  return (
    <input
      className={cx({
        input: true,
        input_error: touched && error,
      })}
      {...input}
      type={type}
      id={id}
      disabled={disabled}
      placeholder={placeholder}
    />
  );
};

export default Input;
