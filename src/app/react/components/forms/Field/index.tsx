import * as React from 'react';
import { Field } from 'react-final-form';

import FieldLabel from '../FieldLabel';
import FieldError from '../FieldError';

import { IOption } from '../Select';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  id: string;
  name: string;
  label?: string;
  required?: boolean;
  type?: string;
  validate?: any;
  cols?: boolean;
  cols50?: boolean;
  placeholder?: string;
  options?: IOption[];
  includeDates?: string[];
  component: any;
  children?: any;
  onFocus?: any;
  touched?: boolean;
  disabled?: boolean;
  error?: string;
  value?: any;
  mask?: string;
}

class FormItem extends React.Component<IProps> {
  render() {
    const { id, label, required, component, cols, cols50, children } = this.props;
    const Component = component;

    return (
      <div
        className={cx({
          field: true,
          field_cols: cols,
          field_cols50: cols50,
        })}
      >
        {label && 
          <div
            className={cx({
              field__label: true,
              field__label_cols: cols,
              field__label_cols50: cols50,
            })}
          >
            <FieldLabel required={required} htmlFor={id} label={label} />
          </div>
        }
        
        <Field {...this.props}>
          {props => {
            const isTouched = this.props.touched || props.meta.touched;
            return (
              <div
                className={cx({
                  field__input: true,
                  field__input_cols: cols,
                  field__input_cols50: cols50,
                })}
              >
                <Component {...props} content={children} />
                <FieldError touched={isTouched} error={props.meta.error || this.props.error} />
              </div>
            );
          }}
        </Field>
      </div>
    );
  }
}

export default FormItem;
