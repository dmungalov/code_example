import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

type IProps = {
  id: string;
  title?: string;
  content?: any;
} & FieldRenderProps;

const Checkbox: React.SFC<IProps> = props => {
  const { id, input, meta: { touched, error }, title, content } = props;

  return (
    <label 
      className={cx({
        'checkbox': true,
        'checkbox_error': touched && error, 
      })}>
      <input className={cx('checkbox__input')} {...input} type="checkbox" id={id} />
      <span className={cx('checkbox__label')}>{title || content}</span>
    </label>
  );
};

export default Checkbox;
