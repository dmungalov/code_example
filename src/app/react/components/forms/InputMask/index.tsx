import * as React from 'react';
import { FieldRenderProps } from 'react-final-form';
import * as MaskedInput from 'react-input-mask/dist/react-input-mask.min';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

type IProps = {
  placeholder?: string;
  id: string;
  type?: string;
  mask: string;
} & FieldRenderProps;

const InputMask: React.SFC<IProps> = props => {
  const { input, meta: { touched, error }, placeholder, id, mask } = props;

  return (
    <MaskedInput
      className={cx({
        'input-mask': true,
        'input-mask_error': touched && error,
      })}
      {...input}
      id={id}
      placeholder={placeholder}
      mask={mask}
      maskChar=""
    />
  );
};

export default InputMask;
