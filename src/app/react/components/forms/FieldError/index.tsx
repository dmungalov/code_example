import * as React from 'react';

import Text, { Size, Style } from '../../Text';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  touched: boolean;
  error?: string;
  warning?: string;
}

class FormError extends React.Component<IProps> {
  render() {
    const { touched, error, warning } = this.props;
    const isShowMessage = touched && (error || warning);
    const styleError = isShowMessage && (error ? 'error' : 'warning');

    return (
      <div
        className={cx({
          errors: true,
          errors_active: isShowMessage,
        })}
      >
        {isShowMessage && (
          <Text size={Size.sm} style={Style[styleError]}>
            {error || warning}
          </Text>
        )}
      </div>
    );
  }
}

export default FormError;
