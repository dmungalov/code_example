import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  label: string;
  htmlFor: string;
  required?: boolean;
}

class FormLabel extends React.Component<IProps> {
  render() {
    const { required, htmlFor, label } = this.props;
    return (
      <label
        className={cx({
          label: true,
          label_required: required,
        })}
        htmlFor={htmlFor}
      >
        {label}
      </label>
    );
  }
}

export default FormLabel;