import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
export const cx = classNames.bind(style);

export enum TitleSize {
  h1 = 'h1', 
  h2 = 'h2', 
  h3 = 'h3', 
  h4 = 'h4',
  h5 = 'h5',
  h6 = 'h6',
};

export enum TitleAlign {
  left = 'left',
  center = 'center',
  right = 'right',
}

export enum TitleWeight {
  normal = 'normal',
  bold = 'bold',
}

interface IProps {
  size?: TitleSize;
  align?: TitleAlign;
  weight?: TitleWeight;
  children: string;
  className?: string;
}

class Title extends React.PureComponent<IProps> {
  render() {
    const { size = TitleSize.h1, align, weight, children, className } = this.props;
    const Tag = size;

    return (
      <Tag className={cx(className, {
        title: true,
        [`title_size_${size}`]: size,
        [`title_align_${align}`]: align,
        [`title_weight_${weight}`]: weight,
      })}>{children}</Tag>
    );
  }
}

export default Title;
