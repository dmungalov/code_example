import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  className?: string;
  children?: any;
  onClick?: any;
  type?: string;
  transparent?: boolean
  disabled?: boolean;
}

class Button extends React.PureComponent<IProps> {
  render() {
    const { children, className, onClick, disabled, type, transparent} = this.props;

    return (
      <button type={type || 'submit'} className={cx(className, {
        button: true,
        button_transparent: transparent,
      })} onClick={onClick} disabled={disabled}>
        {children}
      </button>
    );
  }
}

export default Button;
