import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
export const cx = classNames.bind(style);

export enum MessageType {
  info = 'info',
  warning = 'warning',
  error = 'error',
  success = 'success',
}

interface IProps {
  type?: MessageType;
  children: string;
  className?: string;
}

class Message extends React.PureComponent<IProps> {
  render() {
    const { type = MessageType.info, children, className } = this.props;

    return (
      <div className={cx(className, {
        message: true,
        [`message_type_${type}`]: type,
      })}>{children}</div>
    );
  }
}

export default Message;
