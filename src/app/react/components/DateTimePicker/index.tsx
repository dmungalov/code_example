import * as React from 'react';
import * as moment from 'moment';
import * as ReactSwipe from 'react-swipe';
import adaptiveService from '../../../services/adaptive-service';
import { IPreparedDate, IPreparedTime } from '../../models/checkout';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

const LOCALE_SETTINGS = {
  monthsShort: 'янв_фев_март_апр_мая_июня_июля_авг_сен_окт_ноя_дек'.split('_'),
  weekdays: 'воскр._понед._вторник_среда_четверг_пятница_суббота'.split('_'),
  weekdaysMin: 'вс_пн_вт_ср_чт_пт_сб'.split('_'),
};

moment.locale('ru', LOCALE_SETTINGS);

interface IProps {
  dates: IPreparedDate[];
  selectedDate: {
    date: string;
    from: string;
    to: string;
  };
  onChange: (data: any) => void;
}

interface IState {
  activeDateIndex: number;
  selectDate: {
    from: string;
    to: string;
    date: string;
    presentionalDate: string;
  };
}

class DateTimePicker extends React.Component<IProps, IState> {
  private slider;

  constructor(props) {
    super(props);

    this.state = {
      activeDateIndex: 0,
      selectDate: null,
    };
  }

  componentDidMount() {
    if (adaptiveService.isMobile) {
      const { dates, selectedDate } = this.props;
      const index = dates.findIndex(item => item.date === selectedDate.date);
      this.slider.slide(index, 1)
    }
  }

  sliderNext = () => this.slider.next();
  sliderPrev = () => this.slider.prev();
  sliderToIndex = index => () => this.slider.slide(index);

  sliderOnSwipe = index => this.setState({ activeDateIndex: index });

  isSelectDate = time => {
    const { selectedDate } = this.props;
    return selectedDate.date === time.date && selectedDate.from === time.from && selectedDate.to === time.to;
  }

  renderTimeButton = (index: number, time: IPreparedTime, date: string, label: string) => {
    const objDate = {
      date,
      presentionalDate: time.presentionalDate,
      from: time.from,
      to: time.to,
    };
    const isDisabled = time.limit === 0;
    return (
      <button
        key={index}
        onClick={this.selectTime(objDate)}
        type="button"
        className={cx({
          slots__time: true,
          slots__time_disabled: isDisabled,
          slots__time_current: this.isSelectDate(objDate),
        })}
        disabled={isDisabled}
      >
        {label}
      </button>
    );
  };

  renderColsForDesktop = (list: IPreparedDate[]) => {
    return list.map((item, index) => (
      <div className={cx('slots__day')} key={index}>
        <span className={cx('slots__day-week')}>{item.weekDay}</span>
        <span className={cx('slots__day-calend')}>
          <span className={cx('slots__day-date')}>{item.dateD}</span>
          <span className={cx('slots__day-month')}>{item.dateMMMM}</span>
        </span>
        {item.times.map((time, indexTime) => this.renderTimeButton(indexTime, time, item.date, time.label))}
      </div>
    ));
  };

  renderColsForMobile = (list: IPreparedDate[]) => {
    const { activeDateIndex } = this.state;
    return (
      <div className={cx('slots')}>
        <div className={cx('slots__days')}>
          {list.map((item, index) => (
            <div
              key={index}
              className={cx({
                slots__day: true,
                slots__day_current: index === activeDateIndex,
              })}
              onClick={this.sliderToIndex(index)}
            >
              <span className={cx('slots__day-week')}>{item.weekDayMobile}</span>
              <span className={cx('slots__day-date')}>{item.dateD}</span>
            </div>
          ))}
        </div>
        <div className={cx('slots__times')}>
          <ReactSwipe ref={slider => (this.slider = slider)} swipeOptions={{ callback: this.sliderOnSwipe }}>
            {list.map((item, index) => (
              <div key={index} className={cx('slots__times-wrap')}>
                {item.times.map((time, indexTime) =>
                  this.renderTimeButton(indexTime, time, item.date, time.labelMobile),
                )}
              </div>
            ))}
          </ReactSwipe>
        </div>
        <div className={cx('slots__left')} onClick={this.sliderPrev} />
        <div className={cx('slots__right')} onClick={this.sliderNext} />
      </div>
    );
  };

  selectTime = data => () => {
    this.setState({ selectDate: data });
    this.props.onChange && this.props.onChange(data);
  };

  render() {
    const { dates } = this.props;

    return (
      <div className={cx('datetimepicker')}>
        <div className={cx('slots')}>
          <div className={cx('slots__table')}>
            {!adaptiveService.isMobile ? this.renderColsForDesktop(dates) : this.renderColsForMobile(dates)}
          </div>
        </div>
      </div>
    );
  }
}

export default DateTimePicker;
