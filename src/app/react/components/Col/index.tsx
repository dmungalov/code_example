import * as React from 'react';

/* Styles */
const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
  size: number;
  className?: string;
}

const Col: React.SFC<IProps> = props => (
  <div
    className={cx(props.className, {
      [`col-${props.size}`]: true,
    })}
  >
    {props.children}
  </div>
);

export default Col;
