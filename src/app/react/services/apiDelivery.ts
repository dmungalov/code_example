import * as axios from 'axios';

const initState = window.initState;

export const API_DELIVERY_URL = initState && initState.magdelivery && initState.magdelivery.url || '';
export const API_DELIVERY_TOKEN = initState && initState.magdelivery && initState.magdelivery.token || 1;

const apiDelivery = (axios as any).create({
  baseURL: API_DELIVERY_URL,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'X-Client-Key': API_DELIVERY_TOKEN,
  },
  withCredentials: false,
});

export default apiDelivery;