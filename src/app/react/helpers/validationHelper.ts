const config = {
  required: {
    rule: value => value && value !== '',
    message: 'Это поле обязательно для заполнения.',
  },
  onlyLetters: {
    rule: value => value && /^[a-zA-Zа-яёА-ЯЁ\`\-\s]*$/i.test(value),
    message: 'Поле может содержать только буквы, тире и апостроф',
  },
  email: {
    rule: value => value && /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i.test(value),
    message: 'Пожалуйста, введите правильный адрес электронной почты (email). Например, ivanivanov@domain.com.',
  },
  phone: {
    rule: value => value && /^((\+8|8|\+7|7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10}$/i.test(value),
    message: 'Введите корректный номер телефона',
  },
  password: {
    rule: value => value && (value.length > 5 && value.length === value.trim().length),
    message: 'Пожалуйста, введите не менее 6 символов без пробелов в конце и в начале.',
  },
  minLength: {
    rule: min => value => value ? value.length >= min : true,
    message: min => `Пожалуйста, введите не менее ${min} символов без пробелов в конце и в начале.`,
  },
  maxLength: {
    rule: max => value => value ? value.length <= max : true,
    message: max => `Максимум ${max} символов`,
  },
  isEqual: {
    rule: field => (value, allValues) => value && value === allValues[field],
    message: 'Поля не совпадают',
  },
  phoneOrEmail: {
    rule: value => config.email.rule(value) || config.phone.rule(value),
    message: 'Введите номер телефона или электронную почту',
  },
  needCheck: {
    rule: field => value => value && value.length > 0,
    message: 'Это поле обязательно для заполнения.',
  },
};

export const composeValidators = (...validators) => (value, allValues) =>
  validators.reduce((error, validator) => error || validator(value, allValues), undefined);

const validation = name => (data?: { message?: string, param?: any }) => (value, allValues) => {
  const params = data || {};
  const message = params.message || config[name].message;
  const rule = params.param ? config[name].rule(params.param) : config[name].rule;
  return !rule(value, allValues) ? (typeof message === 'function' ? message(params.param) : message) : undefined;
}

export const required = validation('required');
export const email = validation('email');
export const phone = validation('phone');
export const password = validation('password');
export const minLength = validation('minLength');
export const maxLength = validation('maxLength');
export const isEqual = validation('isEqual');
export const needCheck = validation('needCheck');

export default {
  required: validation('required')(),
  onlyLetters: validation('onlyLetters')(),
  email: validation('email')(),
  phone: validation('phone')(),
  password: validation('password')(),
  phoneOrEmail: validation('phoneOrEmail')(),
  isEqual: validation('isEqual')(),
  needCheck: validation('needCheck')(),
}