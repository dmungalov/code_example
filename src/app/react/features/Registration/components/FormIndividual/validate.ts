import Validation, { composeValidators, minLength, isEqual, needCheck } from '../../../../helpers/validationHelper';

export default {
  ...Validation,
  minLength6: minLength({
    param: 6,
    message: 'Минимальная длина пароля: 6 символов',
  }),
  isEqualToPass: isEqual({
    param: 'password',
    message: 'Пожалуйста, убедитесь, что ваши пароли совпадают.',
  }),
  isNeedCheck: needCheck({
    param: 'rules',
    message: 'Необходимо принять условия поставки',
  }),
  composeValidators,
}
