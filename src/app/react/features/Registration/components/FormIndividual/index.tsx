import * as React from 'react';
import Title, { TitleSize, TitleWeight } from '../../../../components/Title';
import Row from '../../../../components/Row';
import Col from '../../../../components/Col';
import { Form, Input, Checkbox, InputPhone } from '../../../../components/forms';
import Button from '../../../../components/Button';
import Validation from './validate';
const { composeValidators, required, email, phone, isNeedCheck, minLength6, isEqualToPass } = Validation;
import Agreement from '../../../../components/Agreement';
import { ILink } from '../../../../models/registration';

/* Styles */
const style = require('../../style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
    isSubmiting: boolean;
    links: ILink[];
    contract: ILink;
    form: any;
}

const FormIndividual: React.SFC<IProps> = props => {
    const { isSubmiting, links, contract } = props;

    const onSubmit = e => props.form.handleSubmit(e);

    return(
        <form onSubmit={onSubmit}>
            <Form.Section>
                <Title className={cx('title')} size={TitleSize.h3} weight={TitleWeight.bold}>
                    Контактные данные
                </Title>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="name"
                            name="name"
                            label="Имя"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="surname"
                            name="surname"
                            label="Фамилия"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="phone"
                            name="phone"
                            label="Телефон"
                            type="tel"
                            required
                            cols50
                            component={InputPhone}
                            validate={composeValidators(required, phone)}
                        />
                    </Col>
                </Row>
            </Form.Section>
            <Form.Section>
                <Title className={cx('title')} size={TitleSize.h3} weight={TitleWeight.bold}>
                    Информация для авторизации
                </Title>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="email"
                            name="email"
                            label="Электронная почта"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required, email)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="password"
                            name="password"
                            type="password"
                            label="Пароль"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required, minLength6)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="passwordRepeat"
                            name="passwordRepeat"
                            type="password"
                            label="Повторите пароль"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required, isEqualToPass)}
                        />
                    </Col>
                </Row>
            </Form.Section>
            <Form.Section>
                <Row>
                    <Col size={12}>
                        <div className={cx('column')}>
                            <Form.Field  
                                value="1" 
                                name="agree" 
                                id="agree"
                                type="checkbox" 
                                component={Checkbox} >
                                Согласен(а) получать выгодные предложения на покупку продуктов
                            </Form.Field>
                        </div>
                        <div className={cx('column')}>
                            <Form.Field  
                                value="1" 
                                name="rules" 
                                id="rules"
                                type="checkbox" 
                                component={Checkbox}
                                validate={composeValidators(isNeedCheck)} >
                                Я ознакомлен(а) и согласен(а) с <a href={contract.url} target="_blank" className="link">{contract.title}</a>
                            </Form.Field>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <div className={cx('column')}>
                            <Agreement links={links} button="Зарегистрироваться" />
                        </div>
                    </Col>
                </Row>
            </Form.Section>
            <Button className={cx('registration__submit')} disabled={isSubmiting}>
                {isSubmiting ? 'Регистрация...' : 'Зарегистрироваться'}
            </Button>
        </form>
    );
};

export default FormIndividual;