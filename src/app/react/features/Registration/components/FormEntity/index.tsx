import * as React from 'react';
import { OnChange } from 'react-final-form-listeners';
import Title, { TitleSize, TitleWeight } from '../../../../components/Title';
import Row from '../../../../components/Row';
import Col from '../../../../components/Col';
import { Field } from 'react-final-form';
import { Form, Input, Radio, Checkbox, InputPhone, Textarea, InputMask } from '../../../../components/forms';
import Button from '../../../../components/Button';
import Validation from './validate';
const { composeValidators, required, email, phone, isNeedCheck, isEqualToPass } = Validation;
import Agreement from '../../../../components/Agreement';
import { ILink } from '../../../../models/registration';

/* Styles */
const style = require('../../style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
    form: any;
    type: string;
    updateFormState: (data: any) => void;
    isSubmiting: boolean;
    links: ILink[];
    contract: ILink;
}

const FormEntity: React.SFC<IProps> = props => {
    const { isSubmiting, links, type, contract, updateFormState } = props;

    const onSubmit = e => props.form.handleSubmit(e);
    const onChangeCustomerType = value => updateFormState(value);

    return(
        <form onSubmit={onSubmit} >
            <OnChange name="type">{onChangeCustomerType}</OnChange>    
            <Form.Section>
                <Title className={cx('title')} size={TitleSize.h3} weight={TitleWeight.bold}>
                    Информация об организации
                </Title>
                <Row>
                    <Col size={12}>
                        <div className={cx('column')}>
                            <Field  key="3"
                                    value="3" 
                                    name="type" 
                                    type="radio"
                                    component={Radio}>
                                Юридическое лицо
                            </Field>
                            <Field  key="2"
                                    value="2" 
                                    name="type" 
                                    type="radio" 
                                    component={Radio}>
                                Индивидуальный предприниматель
                            </Field>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="orgname"
                            name="orgname"
                            label="Наименование организации"
                            required
                            cols50
                            placeholder={'ООО "Ромашка"'}
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="inn"
                            name="inn"
                            label="ИНН"
                            required
                            cols50
                            mask={type === '3' ? '9999999999' : '999999999999' }
                            component={InputMask}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                {type === '3' && 
                    <Row>
                        <Col size={12}>
                            <Form.Field
                                id="kpp"
                                name="kpp"
                                label="КПП"
                                required
                                cols50
                                mask="999999999"
                                component={InputMask}
                                validate={composeValidators(required)}
                            />
                        </Col>
                    </Row>
                }
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="addresslegal"
                            name="addresslegal"
                            label="Юридический адрес"
                            required
                            cols50
                            component={Textarea}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
            </Form.Section>
                
            <Form.Section>
                <Title className={cx('title')} size={TitleSize.h3} weight={TitleWeight.bold}>
                    Адрес доставки (торговая точка)
                </Title>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="point"
                            name="point"
                            label="Наименование"
                            required
                            placeholder={'Томск, пр. Мира 20, оф. 4'}
                            cols50
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="address"
                            name="address"
                            label="Адрес"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="comment"
                            name="comment"
                            label="Комментарий к адресу"
                            cols50
                            component={Textarea}
                            value="kek"
                        />
                    </Col>
                </Row>
            </Form.Section>

            <Form.Section>
                <Title className={cx('title')} size={TitleSize.h3} weight={TitleWeight.bold}>
                    Контактные данные
                </Title>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="name"
                            name="name"
                            label="Имя"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="surname"
                            name="surname"
                            label="Фамилия"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="phone"
                            name="phone"
                            label="Телефон"
                            type="tel"
                            required
                            cols50
                            component={InputPhone}
                            validate={composeValidators(required, phone)}
                        />
                    </Col>
                </Row>
            </Form.Section>

            <Form.Section>
                <Title className={cx('title')} size={TitleSize.h3} weight={TitleWeight.bold}>
                    Информация для авторизации
                </Title>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="email"
                            name="email"
                            label="Электронная почта"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required, email)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="password"
                            name="password"
                            type="password"
                            label="Пароль"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required)}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <Form.Field
                            id="passwordRepeat"
                            name="passwordRepeat"
                            type="password"
                            label="Повторите пароль"
                            required
                            cols50
                            component={Input}
                            validate={composeValidators(required, isEqualToPass)}
                        />
                    </Col>
                </Row>
            </Form.Section>
                
            <Form.Section>
                <Row>
                <Col size={12}>
                        <div className={cx('column')}>
                            <Form.Field  
                                value="1" 
                                name="agree" 
                                id="agree"
                                type="checkbox" 
                                component={Checkbox} >
                                Согласен(а) получать выгодные предложения на покупку продуктов
                            </Form.Field>
                        </div>
                        <div className={cx('column')}>
                            <Form.Field  
                                value="1" 
                                name="rules" 
                                id="rules"
                                type="checkbox" 
                                component={Checkbox}
                                validate={composeValidators(isNeedCheck)} >
                                Я ознакомлен(а) и согласен(а) с <a href={contract.url} target="_blank" className="link">{contract.title}</a>
                            </Form.Field>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col size={12}>
                        <div className={cx('column')}>
                            <Agreement links={links} button="Зарегистрироваться" />
                        </div>
                    </Col>
                </Row>
            </Form.Section>
                
            <Button className={cx('registration__submit')} disabled={isSubmiting}>
                {isSubmiting ? 'Регистрация...' : 'Зарегистрироваться'}
            </Button>
        </form>
    );
}

export default FormEntity;