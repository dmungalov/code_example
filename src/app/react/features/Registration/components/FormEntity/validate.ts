import Validation, { composeValidators, isEqual, needCheck } from '../../../../helpers/validationHelper';

export default {
  ...Validation,
  isEqualToPass: isEqual({
    param: 'password',
    message: 'Пожалуйста, убедитесь, что ваши пароли совпадают.',
  }),
  isNeedCheck: needCheck({
    param: 'rules',
    message: 'Необходимо принять условия поставки',
  }),
  composeValidators,
}

