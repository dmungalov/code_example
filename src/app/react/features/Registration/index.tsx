import * as React from 'react';
import createDecorator from 'final-form-focus';
const focusOnErrors = createDecorator();
import { Form as FinalForm } from 'react-final-form';

import { postCustomerRegistration } from '../../../api/customer';
import HistoryService from '../../../services/history-service';
import FormIndividual from './components/FormIndividual';
import FormEntity from './components/FormEntity';
import Title, { TitleSize, TitleWeight } from '../../components/Title';
import Message, { MessageType } from '../../components/Message';
import notice from '../../../components/notice-list';
import { IInitialState } from '../../models/registration';

const style = require('./style.scss');
const classNames = require('classnames/bind');
const cx = classNames.bind(style);

interface IProps {
    type: string;
}

interface IState {
    isSubmiting: boolean;
    type: string;
    initialValues: IInitialState;
    error?: string;
}

export default class Registration extends React.Component<IProps, IState> {
    constructor (props) {
        super(props);
        this.state = {
            isSubmiting: false,
            type: this.props.type,
            initialValues: {
                links: [
                    {
                        title: 'с Регламентом работы сайта',
                        url: 'https://google.com',
                    },
                    {
                        title: 'с Согласием на обработку персональных данных',
                        url: 'https://google.com',
                    },
                ],
                contract: {
                    title: 'условиями Договора купли-продажи',
                    url: 'https://google.com',
                },
                contractEntity: {
                    title: 'условиями Договора поставки',
                    url: 'https://google.com',
                },
                entityRegistrationWarning: 'Внимание! Все заявки на регистрацию контрагентов - индивидуальных предпринимателей и юридических лиц рассматриваются специалистами отдела продаж. Это может занять некоторое время. До тех пор, пока контрагент не зарегистрирован, оформление заказов невозможно. Как правило, рассмотрение заявки занимает не более одного рабочего дня.',
                goBackLink: {
                    title: 'Назад',
                    url: '/',
                },
            },
            error: null,
        }
    }

    preparedSubmitData = data => {
        const agree = data.agree && data.agree.length > 0; 

        return {
            lastname: data.surname,
            firstname: data.name,
            email: data.email,
            phone: data.phone,
            password: data.password,
            adv_customer_type: data.type || '1',
            is_subscribed: agree ? '1' : '0',
            adv_full_name: data.orgname,
            adv_inn: data.inn,
            adv_kpp: data.kpp,
            adv_legal_address: data.addresslegal,
            company: data.point,
            street: data.address,
            comments: data.comment,
        };
    };

    onSubmit = data => {
        this.setState({isSubmiting: true });
        const preparedData = this.preparedSubmitData(data);
        /**
         * удаляет пустые поля
         */
        Object.keys(preparedData).forEach(key => {
            (preparedData[key] === null || preparedData[key] === undefined) && delete preparedData[key];
        });
        postCustomerRegistration(preparedData)
            .then(res => { 
                let  newState = {};

                if (res && res.data) {
                    if (res.data.status === 200) {
                        window.location.href = '/';
                        return;
                    } else if (res.data.errors) {
                        newState = {
                            ...newState, 
                            error: res.data.errors[0].message,
                        };
                    }
                } else {
                    notice.alert({ style: 'error', title: 'Произошла ошибка. Попробуйте позже' });
                }

                newState = {
                    ...newState, 
                    isSubmiting: false,
                };
                this.setState(newState);
            })
            .catch(err => {
                notice.alert({ style: 'error', title: 'Произошла ошибка. Попробуйте позже' });
                this.setState({ isSubmiting: false });
            });
    }

    updateURL = (type: string) => {
        HistoryService.replaceState({type});
    }

    updateFormState = (type: string) => {
        if (type !== this.state.type) {
            this.updateURL(type);
            this.setState({ type });
        }    
    }

    render() {
        const { links, contract, contractEntity, entityRegistrationWarning, goBackLink } = this.state.initialValues;
        const { type, error } = this.state; 
        const isEntity = type && type !== '1';

        const formEntity = 
            <FinalForm onSubmit={this.onSubmit} 
                decorators={[focusOnErrors]}
                initialValues={{type}}
                render={form => <FormEntity 
                                    form={form}
                                    type={type}
                                    isSubmiting={this.state.isSubmiting}
                                    updateFormState={this.updateFormState} 
                                    links={links}
                                    contract={contractEntity} />}
            />;
        const formIndividual = 
            <FinalForm onSubmit={this.onSubmit} 
                decorators={[focusOnErrors]}
                render={form => <FormIndividual 
                                    form={form}
                                    isSubmiting={this.state.isSubmiting}
                                    links={links}
                                    contract={contract} />}
            />;

        const form = isEntity ? formEntity : formIndividual;
        const title = isEntity ? 'Регистрация организации' : 'Регистрация';

        return (
            <div className={cx('registration')}>
                <Title className={cx('registration__title')} size={TitleSize.h1} weight={TitleWeight.bold}>{title}</Title>

                <a href={goBackLink.url} className={cx('registration__back')}>{goBackLink.title}</a>

                {isEntity && <Message className={cx('registration__message')} type={MessageType.error}>{entityRegistrationWarning}</Message>}

                {error && <Message className={cx('registration__message')} type={MessageType.error}>{error}</Message>} 

                {form}
            </div>
        );
    }
}