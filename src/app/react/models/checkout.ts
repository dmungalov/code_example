interface IProfile {
  customer_id: string | null;
  firstname?: string;
  lastname?: string;
  email?: string;
  phone?: string;
  adv_customer_type?: string;
  magdelivery_group_id?: any;
}

interface ILink {
  title: string;
  url: string;
}

interface IAddress {
  id: string;
  isDefaultShipping: boolean;
  street: string;
  porch: string | null;
  floor: string | null;
  firstname: string;
  lastname: string;
  telephone: string;
  geo_lat: number;
  geo_lon: number;
  comments: string | null;
}

interface ICartItem {
  quote_item_id: string;
  cost: number;
  qty: number;
  name: string;
  price: number;
  is_weight: boolean;
}

interface ICart {
  quote: {
    list: ICartList;
  },
  total_price: number;
}

interface IMagDelivery {
    url: string;
    token: string;
}

interface IPaymentType {
  id: string;
  title: string;
  dates: string[];
}

interface IWarehouse {
  warehouse_id: number;
  magdelivery_warehouse_id: number;
  geo_lat: number;
  geo_lon: number;
}

// type IAddressList = IAddress[];
// type ILinkList = ILink[];
type ICartList = ICartItem[];

export interface IInitialState {
  magdelivery: IMagDelivery;
  payment_types: IPaymentType[];
  warehouse: IWarehouse;
  profile: IProfile;
  cart: ICart;
  links: ILink[];
  addresses?: IAddress[];
  formTitle: string;
}

// Delivery Dates

interface ISlot {
  from: string;
  to: string;
  limit: number;
  cost: number;
  warehouseId: number;
}

export interface ISlotList {
  [key: string]: ISlot[];
}

interface ISlot {
  from: string;
  to: string;
  limit: number;
  cost: number;
  warehouseId: number;
}

export type IPreparedTime = {
  label: string;
  labelMobile: string;
  presentionalDate: string;
} & ISlot;

export interface IPreparedDate {
  date: string;
  dateD: string;
  dateMMMM: string;
  weekDay: string;
  weekDayMobile: string;
  times: IPreparedTime[];
}