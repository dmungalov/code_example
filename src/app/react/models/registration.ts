export interface ILink {
    title: string;
    url: string;
}

export interface IInitialState {
    links: ILink[];
    contract: ILink;
    contractEntity: ILink;
    entityRegistrationWarning: string;
    goBackLink: ILink;
}