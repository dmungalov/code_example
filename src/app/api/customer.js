import request, { objectToFormData } from '../utils/request';

/**
 * Метод регистрации нового пользователя 
 * adv_customer_type: '1' - физлицо, '2' - ИП, '3' - юрик
 * '1' data = { lastname, firstname, email, phone, password, is_subscribed?, adv_customer_type } - все string
 * '2' data = { lastname,firstname, email, phone, password, is_subscribed?, adv_customer_type, adv_full_name, adv_inn, adv_legal_address, company, street, comments? } - все string
 * '3' data = { lastname,firstname, email, phone, password, is_subscribed?, adv_customer_type, adv_full_name, adv_inn, adv_kpp, adv_legal_address, company, street, comments? } - все string
 * @param data
 */
export const postCustomerRegistration = (data) => request.post('/api/customer/registration', objectToFormData(data));