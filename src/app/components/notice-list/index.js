import './style.scss';
import Notice, {template as noticeTemplate} from '../notice';
import adaptive, {DEVICE_TYPE} from 'services/adaptive-service';

export function noticeListTemplate () {
    return `<div class="notice-list j_notice_list"></div>`;
}

const MAX_MESSAGES_IN_STACK = 5;
const MAX_MESSAGES_IN_STACK_MOBILE = 1;

let instance = null;

class NoticeList {
    constructor () {
        if (!instance) {
            instance = this;
        }

        this.blockSelector = '.j_notice_list';
        this.noticeScrollClass = 'notice-list_scroll';
        this.init();

        return instance;
    }

    init () {
        if (0 === jQuery(this.blockSelector).length) {
            jQuery('body').append(noticeListTemplate());
        }
        this.$block = jQuery(this.blockSelector);
        this.bind();
        this.checkMessages();

        jQuery(window).trigger('scroll');
    }

    checkMessages () {
        if ('undefined' !== typeof page && page.messages) {
            page.messages.forEach((message) => {
                this.alert({title: message, style: 'error'});
            });
        }
    }

    bind () {
        jQuery(document)
            .off('notification.open.listener')
            .on('notification.open.listener', (event, text, style) => {
                this.open({text, style});
            });

        jQuery(window)
            .off('scroll.noticePageScroll')
            .on('scroll.noticePageScroll', this._scrollPageHandler.bind(this));
    }

    alert (options) {
        this.open(options);
    }

    error (options) {
        options.style = 'error';
        this.open(options);
    }

    success (options) {
        options.style = 'success';
        this.open(options);
    }

    warning (options) {
        options.style = 'warning';
        this.open(options);
    }

    info (options) {
        options.style = 'info';
        this.open(options);
    }

    open (options) {
        let $noticeNew = jQuery(noticeTemplate(options)),
            $notice = jQuery('.j_notice'),
            noticeAmount = $notice.length,
            max = adaptive.isMobile ? MAX_MESSAGES_IN_STACK_MOBILE : MAX_MESSAGES_IN_STACK;

        jQuery(this.blockSelector).prepend($noticeNew);

        if (max <= noticeAmount) {
            $notice.last().remove();
        }
        new Notice($noticeNew).open();
    }

    _scrollPageHandler () {
        if (adaptive.isMobile) {
            return;
        }

        let $window = jQuery(window);

        if (97 < $window.scrollTop()) {
            this.$block.addClass(this.noticeScrollClass);
        } else {
            this.$block.removeClass(this.noticeScrollClass);
        }
    }
}

export default new NoticeList;