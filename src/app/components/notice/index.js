import './style.scss';
import Hammer from 'hammerjs';
import adaptiveService from 'services/adaptive-service';


export function template (options) {
    let messages = '';

    if (options.messages && options.messages.length > 0) {
        options.messages.forEach((message) => {
            messages += `<div class="notice__text">${message}</div>`;
        });
    }

    return `
        <div class="notice notice_style_${options.style} j_notice">
            <div class="notice__title">${options.title}</div>
            <div class="notice__close j_notice_close"></div>
            ${messages}
        </div>
    `;
}


export default class Notice {
    constructor ($notice) {
        this.noticeSelecor = '.j_notice';
        this.closeButtonSelecor = '.j_notice_close';
        this.animationClass = 'notice_animation';
        this.closingClass = 'notice_closing';
        this.SPEED = 500;
        this.DELAY = 5000;

        this.$notice = $notice;
        this.$notice.data('instance', this);

        this.init();
    }

    init () {
        this.$closeButton = this.$notice.find(this.closeButtonSelecor);
        this.bind();

        if (adaptiveService.isMobile) {
            this.initMobileAction();
        }
    }

    initMobileAction () {
        let hammer = new Hammer(this.$notice[0]);

        hammer.get('swipe').set({direction: Hammer.DIRECTION_VERTICAL});
        hammer.on('swipeup', this.close.bind(this));
    }

    bind () {
        this.$closeButton
            .off('click.closeNotice')
            .on('click.closeNotice', () => {
                this.$notice.remove();
            });

        this.$notice
            .off('mouseover.clearTimeoutOnMouseOverNotice')
            .on('mouseover.clearTimeoutOnMouseOverNotice', () => {
                if (this.$notice.hasClass(this.closingClass)) {
                    this.$notice.stop(true).removeClass(this.closingClass);
                    this.open();
                }
                this._timeoutClear();
            })
            .off('mouseout.setTimeoutOnMouseOutOfNotice')
            .on('mouseout.setTimeoutOnMouseOutOfNotice', this._timeoutSet.bind(this));
    }

    close () {
        this.$notice
            .addClass(this.animationClass)
            .addClass(this.closingClass);

        this.$notice.removeClass('notice_open');
        setTimeout(() => {
            this.$notice
                .removeClass(this.animationClass)
                .removeClass(this.closingClass);

            this.$notice.remove();
        }, 200);

        jQuery(document).trigger('notice.close', this);
    }

    open () {
        this.$notice.addClass(this.animationClass);

        setTimeout(() => {
            this.$notice
                .addClass('notice_open')
                .removeClass(this.animationClass);

            this._timeoutSet();
        }, 200);

        jQuery(document).trigger('notice.open', this);
    }

    render (options) {
        return template(options);
    }

    destroy () {
        this.$notice.remove();
    }

    _timeoutSet () {
        this.noticeHidingTimer = setTimeout(this.close.bind(this), this.DELAY);
    }

    _timeoutClear () {
        clearTimeout(this.noticeHidingTimer);
    }

    _timeoutReset () {
        this._timeoutClear();
        this._timeoutSet();
    }
}

jQuery(() => {
    let $notices = jQuery('.j_notice');

    if (0 !== $notices.length) {
        $notices.each(function () {
            let $notice = jQuery(this);

            new Notice($notice);
        });
    }
});