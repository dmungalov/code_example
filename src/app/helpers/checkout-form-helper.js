export const getFirstEnabledSlot = slots => {
    let slot = slots.filter(element => element.disabled === 0)[0];
    return slot ? slot.id : undefined;
};
export const addSelectedFlag = (slots, time) => {
    return slots.map(element => element.id === time ? Object.assign(element, {selected: true}) : element);
};
export const isTimeAvailable = (slots, time) => {
    let slot = slots.filter(slot => slot.id === time)[0];
    return slot ? !slot.disabled : false;
};
export const prepareSlots = (slots) => {
    return slots.map(slot => {
        return {
            id: slot.time_id,
            text: slot.start + '-' + slot.end + ' ' + (slot.disabled ? ' недоступно' : ''),
            html: slot.start + '-' + slot.end + ' ' + (slot.disabled ? '<span class="select2-results__unavailable">недоступно</span>' : ''),
            disabled: slot.disabled,
            selected: slot.selected
        };
    });
};