const __svg__ = {
  path: '../**/s-icon-*.svg',
  name: 'img/sprite.svg',
};

window.onload = function () {
  require('webpack-svgstore-plugin/src/helpers/svgxhr')(__svg__);
};
