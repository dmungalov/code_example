export function searchParamsToObject (urlSearch) {
  return (urlSearch || document.location.search).replace(/(^\?)/, '').split('&').map(function (n) {
    return n = n.split('='), this[n[0]] = n[1], this;
  }.bind({}))[0];
}

export function objectToSearchParams (object) {
  let getData = '';

  Object.keys(object).forEach((name) => {
    getData += name + '=' + object[name] + '&';
  });

  return getData.slice(0, -1);
}