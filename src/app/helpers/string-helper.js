export function priceFormat (n) {
  return n
      .toFixed(2)
      .replace(/\./g, ',')
      .replace(/./g, function(c, i, a) {
          return i && c !== "," && ((a.length - i) % 3 === 0) ? ' ' + c : c;
      }) + ' ₽';
}
