let webpack = require('webpack');
let path = require('path');
let autoprefixer = require('autoprefixer');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let currentDate = new Date();

const banner = `Generated on ${currentDate.getDate() + "/" + (currentDate.getMonth() + 1) + "/" + currentDate.getFullYear() + " @ " + currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds()}`;

const ISDEV = process.env.NODE_ENV === 'dev';

const autoprefixerConfig = {
  browsers: [
    'last 3 versions',
    'ie >= 10',
    'iOS >= 8',
    'Safari >= 8',
    'android >= 4',
  ]
};

module.exports = {
  entry: {
    registration: './src/app/registration.js',
  },

  output: {
    publicPath: '/build/',
    path: path.resolve(__dirname, './build/'),
    filename: '[name].js?v=[hash]'
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.es6'],
    modules: [path.resolve(__dirname), 'node_modules', 'app']
  },

  devtool: ISDEV ? 'inline-source-map': false,

  watch: ISDEV,
  watchOptions: {
    aggregateTimeout: 100,
  },

  plugins: [
    new webpack.ProvidePlugin({
        jQuery: 'jquery',
        Promise: 'es6-promise',
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {
        return module.context && module.context.indexOf('node_modules') !== -1;
      }
    }),
    new ExtractTextPlugin('[name].css'),
  ],


  module: {
    rules: [
      {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          enforce: 'pre',
          loader: 'tslint-loader',
          options: {fix: true},
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              silent: true,
            }
          }
        ]
      },
      {
        test: /\.(js|es6)$/,
        use: 'babel-loader',
        include: [path.resolve(__dirname, './src')]
      },
      {
        test: /\.scss$/,
        exclude: [/node_modules/, /react/],
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {loader: 'css-loader', options: {minimize: !ISDEV}},
            {loader: 'postcss-loader', options: {plugins: [autoprefixer(autoprefixerConfig)]}},
            {loader: 'sass-loader'}
          ],
        })
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        include: [/react/],
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[local]__[hash:base64:5]',
                minimize: !ISDEV
              }
            },
            {loader: 'postcss-loader', options: {plugins: [autoprefixer(autoprefixerConfig)]}},
            {loader: 'sass-loader'}
          ],
        })
      },
      {
        test: /\.less$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {loader: 'css-loader', options: {minimize: !ISDEV}},
            {loader: 'postcss-loader', options: {plugins: [autoprefixer(autoprefixerConfig)]}},
            {loader: 'less-loader'}
          ],
        })
      },
      {
        test: /\.ttf(\?.*)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream&name=fonts/[hash].[ext]'
      },
      {
        test: /\.(ttf|eot|woff2?)(\?[a-z0-9]+)?$/,
        use: 'file-loader?name=font/[name]-[hash].[ext]',
      },
      {
        test: /\.(jpe?g|png|svg|gif)$/i,
        use: ['url-loader?limit=5000&name=img/[name].[ext]'],
      },
    ]
  }
};

if (!ISDEV) {
  module.exports.bail = true;
  module.exports.plugins.push(new webpack.optimize.UglifyJsPlugin({
    mangle: false,
    sourceMap: false,
    output: {comments: false},
    compress: {
      sequences: true,  // join consecutive statemets with the “comma operator”
      properties: true,  // optimize property access: a["foo"] → a.foo
      dead_code: true,  // discard unreachable code
      drop_debugger: true,  // discard “debugger” statements
      unsafe: false, // some unsafe optimizations (see below)
      conditionals: true,  // optimize if-s and conditional expressions
      comparisons: true,  // optimize comparisons
      evaluate: true,  // evaluate constant expressions
      booleans: true,  // optimize boolean expressions
      loops: true,  // optimize loops
      unused: true,  // drop unused variables/functions
      hoist_funs: true,  // hoist function declarations
      hoist_vars: false, // hoist variable declarations
      if_return: true,  // optimize if-s followed by return/continue
      join_vars: true,  // join var declarations
      cascade: true,  // try to cascade `right` into `left` in sequences
      side_effects: true,  // drop side-effect-free statements
      warnings: false,  // warn about potentially dangerous optimizations/code
      global_defs: {}     // global definitions
    }
  }));
  module.exports.plugins.push(new webpack.BannerPlugin({banner: banner}));
}
